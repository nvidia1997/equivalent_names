import React, { useState } from 'react';
import TextContainer from '../../components/TextContainer';
import Selector from '../../components/Selector';
import { getLanguages } from '../../utils/mapper';
import { translateNames, changeNumbersBase } from '../../utils/text-processor';
import OpenFile from '../../components/OpenFile';

function Home() {
  const [originaltext, setOriginalText] = useState('');
  const [editedtext, setEditedText] = useState();
  const languages = getLanguages();
  const [language, setLanguage] = useState(languages[0]);
  const bases = [2, 16, 8, 5, 10];
  const [base, setBase] = useState(bases[0]);

  /**@param {String} text */
  const onTextChange = (text) => {
    setOriginalText(text);
    setEditedText('');
  }

  /**@param {String} nextLanguage */
  const onLanguageChange = (nextLanguage) => {
    setLanguage(nextLanguage);
  }

  /**@param {Number} nextBase */
  const onBaseChange = (nextBase) => {
    setBase(Number(nextBase));
  }

  const onTranslateNames = () => {
    const text = translateNames(originaltext, language);
    setEditedText(text);
  }

  const onConvertNumbers = () => {
    const text = changeNumbersBase(originaltext, base);
    setEditedText(text);
  }

  return (
    <>
      <OpenFile onFileLoad={onTextChange} />
      <p>
        <label>Language :</label>
        <Selector items={languages} onChange={onLanguageChange} />
      </p>
      <p>
        <label>Number base :</label>
        <Selector items={bases} onChange={onBaseChange} />
      </p>

      <p><button onClick={onTranslateNames}>Translate names</button></p>
      <p><button onClick={onConvertNumbers}>Convert numbers</button></p>

      <div className="text-containers-wrapper">
        <TextContainer title='Original text' text={originaltext} onChange={onTextChange} />
        <TextContainer title='Edited text' text={editedtext} />
      </div>
    </>
  );
}

export default Home;