import React from 'react';
import { Route } from 'react-router-dom';
import Home from './pages/Home';

export const ROUTE_PATHS = {
  HOME: '/',
}

export const routes = [
  <Route exact path={ROUTE_PATHS.HOME} component={Home} key={ROUTE_PATHS.HOME} />,
];
