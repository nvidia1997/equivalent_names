import React from 'react';
import { BrowserRouter as Router } from 'react-router-dom';
import { routes } from '../../routes';

import './stylesheets/main.scss';

function App() {
  return (
    <Router basename={process.env.PUBLIC_URL}>
      <div className='app'>
        {routes}
      </div>
    </Router>
  );
}

export default App;
