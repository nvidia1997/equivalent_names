import React, { useState } from 'react';
import PropTypes from 'prop-types';

function Selector(props) {
  /** @type {Array}*/
  const items = props.items;
  const [value, setValue] = useState(items && items[0]);

  const onSelectionChange = (nextValue) => {
    const { onChange } = props;
    if (onChange) { props.onChange(nextValue); }

    setValue(nextValue);
  }

  return (
    <select value={value} onChange={({ target }) => onSelectionChange(target.value)}>
      {items.map(item => <option key={`${item}`}>{item}</option>)}
    </select>
  );
}

Selector.propTypes = {
  items: PropTypes.array.isRequired,
  onChange: PropTypes.func,
}

export default Selector;