import React, { PureComponent } from 'react';
import PropTypes from 'prop-types';

import './stylesheets/main.scss';

class TextContainer extends PureComponent {
  render() {
    const { title, text, onChange } = this.props;

    return (
      <div className="text-container">
        <p>{title}</p>
        <textarea
          readOnly={onChange ? false : true}
          value={text}
          onChange={({ target: { value } }) => onChange && onChange(value)}
        />
      </div>
    );
  }
}

TextContainer.propTypes = {
  title: PropTypes.string.isRequired,
  onChange: PropTypes.func,
  text: PropTypes.string,
}

export default TextContainer;