import React from 'react';
import PropTypes from 'prop-types';

function OpenFile(props) {

  const selectFile = (files) => {
    console.log('selected');

    if (!files) { return; }

    const file = files[0];
    if (!file) { return console.error('file not loaded'); }
    const filereader = new FileReader(file);
    filereader.onload = ({ target: { result: text } }) => props.onFileLoad(text);
    filereader.readAsBinaryString(file);
  }

  return (
    <input
      type="file"
      multiple={false}
      accept=".txt"
      onChange={({ target: { files } }) => props.onFileLoad && selectFile(files)}
    />
  );
}

OpenFile.propTypes = {
  onFileLoad: PropTypes.func.isRequired,
}

export default OpenFile;