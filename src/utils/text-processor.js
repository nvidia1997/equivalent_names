import { getEquivalentName, getNames } from './mapper';
import { convertNumbers } from './number-converter';

/**
 * @param {!String} word
 * @returns {String} 
 */
export const capitalize = (word) => {
  return word && word.charAt(0).toUpperCase() + word.slice(1);
}

/**
 * @param {!String} text 
 * @param {!String} language 
 * @returns {String} 
 */
export const translateNames = (text, language) => {
  let translatedText = text;
  const names = getNames();
  for (const name of names) {
    translatedText = translatedText.replace(name, getEquivalentName(name, language));
  }

  return translatedText;
}

/**
 * @param {!String} text 
 * @returns {Array<Number>} 
 */
export const getAllNumbers = (text) => {
  const matches = text.match(/\d+([/.]\d+)?/g);
  return matches;
}

/**
 * @param {!String} text
 * @param {!Number} base
 * @returns {String} 
 */
export const changeNumbersBase = (text, base) => {
  const numbers = getAllNumbers(text);
  const convertedNumbers = convertNumbers(numbers, base);
  let tempText = text;
  for (let i = 0; i < numbers.length; i++) {
    tempText = tempText.replace(numbers[i], convertedNumbers[i]);
  }

  return tempText;
}
