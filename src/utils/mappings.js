export const mappings = {
  Aaron: [
    {
      German: 'Aaron',
      Dutch: 'Aäron',
      Maltese: 'Aron',
      Latin: 'Aaron',
      French: 'Aaron',
      Italian: 'Aronne',
      Spanish: 'Aarón',
      Catalan: 'Aaró',
      Portuguese: 'Aarão, Arão',
      Romanian: 'Aron',
      
    }
  ],
  Abel: [
    {
      German: 'Abel',
      Dutch: 'Abel',
      Maltese: 'Abel',
      Latin: 'Abel',
      French: 'Abel',
      Italian: 'Abele',
      Spanish: 'Abel',
      Catalan: 'Abel',
      Portuguese: 'Abel',
      Romanian: 'No-Name',
      
    }
  ],
  Abraham: [
    {
      German: 'Abraham',
      Dutch: 'Abraham',
      Maltese: 'Abram',
      Latin: 'Abraham',
      French: 'Abraham',
      Italian: 'Abramo',
      Spanish: 'Abraham',
      Catalan: 'Abraham',
      Portuguese: 'Abraão',
      Romanian: 'Avram',
      
    }
  ],
  Adam: [
    {
      German: 'Adam',
      Dutch: 'Adam',
      Maltese: 'Adam',
      Latin: 'Adamus',
      French: 'Adam',
      Italian: 'Adamo',
      Spanish: 'Adán',
      Catalan: 'Adam',
      Portuguese: 'Adão',
      Romanian: 'Adam',
      
    }
  ],
  Adrian: [
    {
      German: 'Adrian',
      Dutch: 'Adriaan',
      Maltese: 'Adrijanu',
      Latin: 'Adrianus, Hadrianus',
      French: 'Adrien',
      Italian: 'Adriano',
      Spanish: 'Adrián',
      Catalan: 'Adrià',
      Portuguese: 'Adriano, Adrião',
      Romanian: 'Adrian',
      
    }
  ],
  Athanasius: [
    {
      German: 'Athanas',
      Dutch: 'Athanasius',
      Maltese: 'Atanażju',
      Latin: 'Athanasius',
      French: 'Athanase',
      Italian: 'Atanasio',
      Spanish: 'Atanasio',
      Catalan: 'Atanasi',
      Portuguese: 'Afanásio',
      Romanian: 'Atanasie',
      
    }
  ],
  Agatho: [
    {
      German: 'No-Name',
      Dutch: 'Agatho',
      Maltese: 'No-Name',
      Latin: 'Agatho',
      French: 'Agathon',
      Italian: 'Agatone',
      Spanish: 'Agatón',
      Catalan: 'Agató',
      Portuguese: 'Agatão',
      Romanian: 'No-Name',
      
    }
  ],
  'Al(l)an, Allen, Alun': [
    {
      German: 'Alan',
      Dutch: 'Alan',
      Maltese: 'Alan',
      Latin: 'Alanus',
      French: 'Alain',
      Italian: 'Alano',
      Spanish: 'Alano',
      Catalan: 'Alà',
      Portuguese: 'Alan',
      Romanian: 'No-Name',
      
    }
  ],
  Albert: [
    {
      German: 'Albert, Albrecht',
      Dutch: 'Albert, Albrecht',
      Maltese: 'Albertu',
      Latin: 'Albertus',
      French: 'Albert',
      Italian: 'Alberto',
      Spanish: 'Alberto',
      Catalan: 'Albert',
      Portuguese: 'Alberto',
      Romanian: 'Albert',
      
    }
  ],
  'Alexander, Alex': [
    {
      German: 'Alexander, Alex',
      Dutch: 'Alexander',
      Maltese: 'Alessandru',
      Latin: 'Alexander',
      French: 'Alexandre',
      Italian: 'Alessandro',
      Spanish: 'Alejandro',
      Catalan: 'Alexandre, Àlex',
      Portuguese: 'Alexandre',
      Romanian: 'Alexandru',
      
    }
  ],
  Alexis: [
    {
      German: 'Alexius',
      Dutch: 'Alexius',
      Maltese: 'Alessju',
      Latin: 'Alexius',
      French: 'Alexis',
      Italian: 'Alessio',
      Spanish: 'Alejo',
      Catalan: 'Aleix, Àlex',
      Portuguese: 'Aleixo',
      Romanian: 'No-Name',
      
    }
  ],
  Alphonso: [
    {
      German: 'Alfons',
      Dutch: 'Alfons',
      Maltese: 'Alfonsu, Fonzu',
      Latin: 'Alphonsus',
      French: 'Alphonse',
      Italian: 'Alfonso',
      Spanish: 'Alfonso, Alonzo',
      Catalan: 'Alfons',
      Portuguese: 'Alfonso, Afonso',
      Romanian: 'No-Name',
      
    }
  ],
  Alfred: [
    {
      German: 'Alfred',
      Dutch: 'Alfred',
      Maltese: 'Alfredu',
      Latin: 'Alfredus',
      French: 'Alfred',
      Italian: 'Alfredo',
      Spanish: 'Alfredo',
      Catalan: 'Alfred',
      Portuguese: 'Alfredo',
      Romanian: 'Alfred',
      
    }
  ],
  Alvarus: [
    {
      German: 'No-Name',
      Dutch: 'No-Name',
      Maltese: 'No-Name',
      Latin: 'Alvarus',
      French: 'No-Name',
      Italian: 'Alvaro',
      Spanish: 'Álvaro',
      Catalan: 'Àlvar',
      Portuguese: 'Álvaro',
      Romanian: 'No-Name',
      
    }
  ],
  Alvin: [
    {
      German: 'Adelwin',
      Dutch: 'Adelwijn',
      Maltese: 'No-Name',
      Latin: 'No-Name',
      French: 'No-Name',
      Italian: 'No-Name',
      Spanish: 'No-Name',
      Catalan: 'No-Name',
      Portuguese: 'No-Name',
      Romanian: 'No-Name',
      
    }
  ],
  Ambrose: [
    {
      German: 'Ambros',
      Dutch: 'Ambrosius',
      Maltese: 'Ambrożju',
      Latin: 'Ambrosius',
      French: 'Ambroise',
      Italian: 'Ambrosio, Ambrogio',
      Spanish: 'Ambrosio',
      Catalan: 'Ambrós, Ambrosi',
      Portuguese: 'Ambrósio',
      Romanian: 'Ambrozie',
      
    }
  ],
  Anatolius: [
    {
      German: 'No-Name',
      Dutch: 'Anatolius',
      Maltese: 'Anatolju',
      Latin: 'Anatolius',
      French: 'Anatole',
      Italian: 'Anatolio',
      Spanish: 'Anatolio',
      Catalan: 'No-Name',
      Portuguese: 'Anatólio',
      Romanian: 'Anatol',
      
    }
  ],
  Andrew: [
    {
      German: 'Andreas',
      Dutch: 'Andreas, Andries',
      Maltese: 'Andrija, Indrì',
      Latin: 'Andreas',
      French: 'André',
      Italian: 'Andrea',
      Spanish: 'Andrés',
      Catalan: 'Andreu',
      Portuguese: 'André',
      Romanian: 'Andrei',
      
    }
  ],
  Anthony: [
    {
      German: 'Anton',
      Dutch: 'Anton, Antoon',
      Maltese: 'Anton, Antonju',
      Latin: 'Antonius',
      French: 'Antoine',
      Italian: 'Antonio',
      Spanish: 'Antonio, Antón',
      Catalan: 'Antoni, Anton',
      Portuguese: 'António, Antão',
      Romanian: 'Anton',
      
    }
  ],
  Apollinaris: [
    {
      German: 'No-Name',
      Dutch: 'Apollinaris',
      Maltese: 'Apollinarju',
      Latin: 'Apollinaris',
      French: 'Apollinaire',
      Italian: 'Apollinare',
      Spanish: 'Apolinar, Apolinario',
      Catalan: 'Apollinar',
      Portuguese: 'Apolinário',
      Romanian: 'No-Name',
      
    }
  ],
  Apollonius: [
    {
      German: 'No-Name',
      Dutch: 'Apollonius',
      Maltese: 'Apollonju',
      Latin: 'Apollonius',
      French: 'No-Name',
      Italian: 'Apollonio',
      Spanish: 'Apolonio',
      Catalan: 'Apolloni',
      Portuguese: 'Apolónio',
      Romanian: 'No-Name',
      
    }
  ],
  Arcadius: [
    {
      German: 'No-Name',
      Dutch: 'Arcadius',
      Maltese: 'Arkadju',
      Latin: 'Arcadius',
      French: 'Arcade, Arcadius',
      Italian: 'Arcadio',
      Spanish: 'Arcadio',
      Catalan: 'Arcadi',
      Portuguese: 'Arcádio',
      Romanian: 'Arcadie',
      
    }
  ],
  Archibald: [
    {
      German: 'Archibald',
      Dutch: 'Archibald, Erkenbout',
      Maltese: 'Arċibaldu',
      Latin: 'Archibaldus',
      French: 'Archimbaud',
      Italian: 'Arcibaldo',
      Spanish: 'Archibaldo',
      Catalan: 'No-Name',
      Portuguese: 'Arquibaldo',
      Romanian: 'No-Name',
      
    }
  ],
  Aristarchus: [
    {
      German: 'No-Name',
      Dutch: 'Aristarchus',
      Maltese: 'Aristarku',
      Latin: 'Aristarchus',
      French: 'Aristarque',
      Italian: 'Aristarco',
      Spanish: 'Aristarco',
      Catalan: 'No-Name',
      Portuguese: 'Aristarco',
      Romanian: 'No-Name',
      
    }
  ],
  Arnold: [
    {
      German: 'Arnold',
      Dutch: 'Arnold, Arnout',
      Maltese: 'Arnaldu',
      Latin: 'Arnoldus',
      French: 'Arnaud, Arnauld, Arnold',
      Italian: 'Arnoldo, Arnaldo',
      Spanish: 'Arnaldo',
      Catalan: 'Arnald, Arnau',
      Portuguese: 'Arnaldo, Arnoldo',
      Romanian: 'Arnold',
      
    }
  ],
  Arsenius: [
    {
      German: 'No-Name',
      Dutch: 'Arsenius',
      Maltese: 'Arsenju',
      Latin: 'Arsenius',
      French: 'Arsène',
      Italian: 'Arsenio',
      Spanish: 'Arsenio',
      Catalan: 'Arsénio',
      Portuguese: 'Arseni',
      Romanian: 'Arsenie',
      
    }
  ],
  Arthur: [
    {
      German: 'Arthur',
      Dutch: 'Arthur',
      Maltese: 'Arturu',
      Latin: 'Arthurus',
      French: 'Arthur, Arthus',
      Italian: 'Arturo',
      Spanish: 'Arturo',
      Catalan: 'Artur',
      Portuguese: 'Artur',
      Romanian: 'Artur',
      
    }
  ],
  'August, Austin': [
    {
      German: 'August',
      Dutch: 'Augustus',
      Maltese: 'Awgustu',
      Latin: 'Augustus',
      French: 'Auguste',
      Italian: 'Augusto',
      Spanish: 'Agustín',
      Catalan: 'August',
      Portuguese: 'Augusto',
      Romanian: 'Augustin',
      
    }
  ],
  Baldwin: [
    {
      German: 'Balduin',
      Dutch: 'Boudewijn',
      Maltese: 'No-Name',
      Latin: 'Balduinus',
      French: 'Baudoin',
      Italian: 'Balduino, Baldovino',
      Spanish: 'Balduino, Baldovinos',
      Catalan: 'Baldui',
      Portuguese: 'Balduíno',
      Romanian: 'No-Name',
      
    }
  ],
  Basil: [
    {
      German: 'Basilius',
      Dutch: 'Basilius',
      Maltese: 'Bażilju',
      Latin: 'Basilius',
      French: 'Basile',
      Italian: 'Basilio',
      Spanish: 'Basilio',
      Catalan: 'Basili',
      Portuguese: 'Basílio',
      Romanian: 'Vasile',
      
    }
  ],
  'Benedick, Benedict': [
    {
      German: 'Benedikt',
      Dutch: 'Benedikt',
      Maltese: 'Benedittu',
      Latin: 'Benedictus',
      French: 'Benoît, Bénédict',
      Italian: 'Benedetto',
      Spanish: 'Benito, Benedicto',
      Catalan: 'Benet',
      Portuguese: 'Bento, Benedito',
      Romanian: 'No-Name',
      
    }
  ],
  Benjamin: [
    {
      German: 'Benjamin',
      Dutch: 'Benjamin',
      Maltese: 'Benjamin',
      Latin: 'Beniamin',
      French: 'Benjamin',
      Italian: 'Beniamino',
      Spanish: 'Benjamín',
      Catalan: 'Benjamí',
      Portuguese: 'Benjamim',
      Romanian: 'No-Name',
      
    }
  ],
  Bernard: [
    {
      German: 'Bernhard',
      Dutch: 'Bernard',
      Maltese: 'Bernard',
      Latin: 'Bernardus',
      French: 'Bernard',
      Italian: 'Bernardo',
      Spanish: 'Bernardo',
      Catalan: 'Bernat',
      Portuguese: 'Bernardo',
      Romanian: 'No-Name',
      
    }
  ],
  'Bertram, Bertrand': [
    {
      German: 'Bertram',
      Dutch: 'Bertram',
      Maltese: 'Bertram',
      Latin: 'Bertrandus',
      French: 'Bertrand',
      Italian: 'Bertrando',
      Spanish: 'Bertrán, Beltrán',
      Catalan: 'Beltran, Bertran',
      Portuguese: 'Beltrão',
      Romanian: 'No-Name',
      
    }
  ],
  'Blase, Blaise': [
    {
      German: 'Blas',
      Dutch: 'Blas, Bjaġju',
      Maltese: 'Blasius, Blaas',
      Latin: 'Blasius',
      French: 'Blaise',
      Italian: 'Biagio',
      Spanish: 'Blas',
      Catalan: 'Blai, Blasi',
      Portuguese: 'Brás',
      Romanian: 'No-Name',
      
    }
  ],
  Boniface: [
    {
      German: 'Bonifaz, Bonifazius',
      Dutch: 'Bonifatius',
      Maltese: 'Bonifaċju',
      Latin: 'Bonifatius',
      French: 'Boniface',
      Italian: 'Bonifacio',
      Spanish: 'Bonifacio',
      Catalan: 'Bonifaci',
      Portuguese: 'No-Name',
      Romanian: 'No-Name',
      
    }
  ],
  Boris: [
    {
      German: 'Boris',
      Dutch: 'Boris',
      Maltese: 'Boris',
      Latin: 'Borises',
      French: 'Boris',
      Italian: 'Boris',
      Spanish: 'Boris',
      Catalan: 'Boris',
      Portuguese: 'Bóris',
      Romanian: 'No-Name',
      
    }
  ],
  Brian: [
    {
      German: 'Brian',
      Dutch: 'Brian',
      Maltese: 'No-Name',
      Latin: 'Brianus',
      French: 'Brian',
      Italian: 'No-Name',
      Spanish: 'No-Name',
      Catalan: 'No-Name',
      Portuguese: 'No-Name',
      Romanian: 'No-Name',
      
    }
  ],
  Bruno: [
    {
      German: 'Bruno',
      Dutch: 'Bruno, Bruin',
      Maltese: 'No-Name',
      Latin: 'Bruno',
      French: 'Bruno',
      Italian: 'Bruno',
      Spanish: 'Bruno',
      Catalan: 'Bru',
      Portuguese: 'Bruno',
      Romanian: 'No-Name',
      
    }
  ],
  Caesar: [
    {
      German: 'Cäsar',
      Dutch: 'Cesar',
      Maltese: 'Ċesri',
      Latin: 'Caesar',
      French: 'César',
      Italian: 'Cesare',
      Spanish: 'César',
      Catalan: 'Cèsar',
      Portuguese: 'César',
      Romanian: 'Cezar',
      
    }
  ],
  Camillus: [
    {
      German: 'Kamil',
      Dutch: 'Camiel',
      Maltese: 'Kamillu',
      Latin: 'Camillus',
      French: 'Camille',
      Italian: 'Camillo',
      Spanish: 'Camilo',
      Catalan: 'Camil',
      Portuguese: 'No-Name',
      Romanian: 'Camil',
      
    }
  ],
  Casimir: [
    {
      German: 'Kasimir',
      Dutch: 'Casimier',
      Maltese: 'No-Name',
      Latin: 'Casimirus',
      French: 'Casimir',
      Italian: 'Casimiro',
      Spanish: 'Casimiro',
      Catalan: 'Casimir',
      Portuguese: 'Casimiro',
      Romanian: 'Cazimir',
      
    }
  ],
  Cecil: [
    {
      German: 'No-Name',
      Dutch: 'Ceciel',
      Maltese: 'Ċeċilju',
      Latin: 'Caecilius',
      French: 'Cécile',
      Italian: 'Cecilio',
      Spanish: 'Cecilio',
      Catalan: 'Cecili',
      Portuguese: 'Cecílio',
      Romanian: 'Cecil',
      
    }
  ],
  'Charles, Carl': [
    {
      German: 'Karl, Carl',
      Dutch: 'Karel',
      Maltese: 'Karlu',
      Latin: 'Carolus',
      French: 'Charles',
      Italian: 'Carlo',
      Spanish: 'Carlos',
      Catalan: 'Carles',
      Portuguese: 'Carlos',
      Romanian: 'Carol',
      
    }
  ],
  Christian: [
    {
      German: 'Kristian, Christian',
      Dutch: 'Christiaan, Karsten',
      Maltese: 'Kristjanu',
      Latin: 'Christianus',
      French: 'Christian, Chrétien',
      Italian: 'Cristiano',
      Spanish: 'Cristián',
      Catalan: 'No-Name',
      Portuguese: 'Cristiano',
      Romanian: 'Cristian',
      
    }
  ],
  Christopher: [
    {
      German: 'Christoph',
      Dutch: 'Christoffel, Christoffer, Kristoffer',
      Maltese: 'Kristofru',
      Latin: 'Christophorus',
      French: 'Christophe',
      Italian: 'Cristoforo',
      Spanish: 'Cristóbal',
      Catalan: 'Cristòfol, Cristòfor',
      Portuguese: 'Cristóvão',
      Romanian: 'Cristofor',
      
    }
  ],
  Clarence: [
    {
      German: 'No-Name',
      Dutch: 'Clarence',
      Maltese: 'No-Name',
      Latin: 'No-Name',
      French: 'No-Name',
      Italian: 'Clarenzio',
      Spanish: 'Clarencio',
      Catalan: 'Clarenci',
      Portuguese: 'Clarencio',
      Romanian: 'No-Name',
      
    }
  ],
  Claude: [
    {
      German: 'Klaudius, Claudius',
      Dutch: 'Claudius',
      Maltese: 'Klawdju',
      Latin: 'Claudius',
      French: 'Claude',
      Italian: 'Claudio',
      Spanish: 'Claudio',
      Catalan: 'Claudi',
      Portuguese: 'Cláudio',
      Romanian: 'Claudiu',
      
    }
  ],
  Clement: [
    {
      German: 'Klemens, Clemens',
      Dutch: 'Clemens',
      Maltese: 'Kelment, Klement, Kliment',
      Latin: 'Clemens',
      French: 'Clément',
      Italian: 'Clemente',
      Spanish: 'Clemente',
      Catalan: 'Climent',
      Portuguese: 'Clemente',
      Romanian: 'No-Name',
      
    }
  ],
  'Colin, Collin': [
    {
      German: 'Klaus, Claus',
      Dutch: 'Klaas',
      Maltese: 'Kolinu',
      Latin: 'Colandus',
      French: 'Colin',
      Italian: 'Nicolino',
      Spanish: 'Colás',
      Catalan: 'No-Name',
      Portuguese: 'No-Name',
      Romanian: 'No-Name',
      
    }
  ],
  Conrad: [
    {
      German: 'Konrad, Conrad',
      Dutch: 'Koenraad',
      Maltese: 'Konrad, Korradu',
      Latin: 'Conradus',
      French: 'Conrad',
      Italian: 'Corrado',
      Spanish: 'Conrado',
      Catalan: 'Conrad',
      Portuguese: 'Conrado',
      Romanian: 'No-Name',
      
    }
  ],
  Constantine: [
    {
      German: 'Konstantin, Constantin',
      Dutch: 'Constantijn',
      Maltese: 'Kostantinu',
      Latin: 'Constantinus',
      French: 'Constantin',
      Italian: 'Costantino',
      Spanish: 'Constantino',
      Catalan: 'Constantí',
      Portuguese: 'Constantino',
      Romanian: 'Constantin',
      
    }
  ],
  Cornelius: [
    {
      German: 'Kornelius, Cornelius',
      Dutch: 'Cornelis, Kees',
      Maltese: 'Kornelju',
      Latin: 'Cornelius',
      French: 'Corneille',
      Italian: 'Cornelio',
      Spanish: 'Cornelio',
      Catalan: 'Corneli',
      Portuguese: 'Cornélio',
      Romanian: 'Corneliu, Cornel',
      
    }
  ],
  Cyprian: [
    {
      German: 'Zyprian',
      Dutch: 'Cypriaan',
      Maltese: 'Ċiprijanu',
      Latin: 'Cyprianus',
      French: 'Cyprien',
      Italian: 'Cipriano',
      Spanish: 'Ciprián, Cipriano',
      Catalan: 'Ciprià',
      Portuguese: 'Cipriano',
      Romanian: 'Ciprian',
      
    }
  ],
  Cyril: [
    {
      German: 'Kyrill',
      Dutch: 'Cyriel',
      Maltese: 'Ċirillu',
      Latin: 'Cyrillus',
      French: 'Cyrille',
      Italian: 'Cirillo',
      Spanish: 'Cirilo',
      Catalan: 'Cirili',
      Portuguese: 'Cirilo',
      Romanian: 'Chirilǎ',
      
    }
  ],
  Cyrus: [
    {
      German: 'No-Name',
      Dutch: 'Cyrus',
      Maltese: 'Ċiru',
      Latin: 'Cyrus',
      French: 'No-Name',
      Italian: 'Ciro',
      Spanish: 'Ciro',
      Catalan: 'No-Name',
      Portuguese: 'Ciro',
      Romanian: 'No-Name',
      
    }
  ],
  'Damian, Damien': [
    {
      German: 'Damian',
      Dutch: 'Damiaan',
      Maltese: 'Damjanu',
      Latin: 'Damianus',
      French: 'Damien',
      Italian: 'Damiano',
      Spanish: 'Damián',
      Catalan: 'Damià',
      Portuguese: 'Damião',
      Romanian: 'Damian',
      
    }
  ],
  Daniel: [
    {
      German: 'Daniel',
      Dutch: 'Daniël',
      Maltese: 'Danjeli',
      Latin: 'Daniel',
      French: 'Daniel',
      Italian: 'Daniele',
      Spanish: 'Daniel',
      Catalan: 'Daniel',
      Portuguese: 'Daniel',
      Romanian: 'Dǎnilǎ, Dan, Daniel',
      
    }
  ],
  Darius: [
    {
      German: 'Darius',
      Dutch: 'Darius',
      Maltese: 'Darju',
      Latin: 'Dareus',
      French: 'No-Name',
      Italian: 'Dario',
      Spanish: 'Darío',
      Catalan: 'No-Name',
      Portuguese: 'Dário',
      Romanian: 'Darius',
      
    }
  ],
  David: [
    {
      German: 'David',
      Dutch: 'David, Daaf',
      Maltese: 'David',
      Latin: 'David',
      French: 'David',
      Italian: 'Davide',
      Spanish: 'David',
      Catalan: 'David, Davi',
      Portuguese: 'David',
      Romanian: 'No-Name',
      
    }
  ],
  Dennis: [
    {
      German: 'Dennis',
      Dutch: 'Dennis, Denijs',
      Maltese: 'Dijoniżju',
      Latin: 'Dionysius',
      French: 'Denis',
      Italian: 'Dionigi, Dionisio',
      Spanish: 'Dionisio',
      Catalan: 'Dionís',
      Portuguese: 'Dinis, Dionísio',
      Romanian: 'Dionisie',
      
    }
  ],
  Derreck: [
    {
      German: 'No-Name',
      Dutch: 'Diederik, Dirk',
      Maltese: 'No-Name',
      Latin: 'Theoderic',
      French: 'Thierry',
      Italian: 'Teodorico',
      Spanish: 'No-Name',
      Catalan: 'No-Name',
      Portuguese: 'No-Name',
      Romanian: 'No-Name',
      
    }
  ],
  Dimitry: [
    {
      German: 'No-Name',
      Dutch: 'Dimitri',
      Maltese: 'Dimitri',
      Latin: 'Demetrius',
      French: 'Dimitri',
      Italian: 'Demetrio, Dimitri',
      Spanish: 'Demetrio',
      Catalan: 'No-Name',
      Portuguese: 'Demétrio',
      Romanian: 'Dumitru',
      
    }
  ],
  Dominic: [
    {
      German: 'Dominik',
      Dutch: 'Dominiek',
      Maltese: 'Duminku',
      Latin: 'Dominicus',
      French: 'Dominique',
      Italian: 'Domenico',
      Spanish: 'Domingo',
      Catalan: 'Domènec',
      Portuguese: 'Domingos',
      Romanian: 'No-Name',
      
    }
  ],
  Donald: [
    {
      German: 'Donald',
      Dutch: 'Donald',
      Maltese: 'No-Name',
      Latin: 'Donivaldus',
      French: 'Donald',
      Italian: 'No-Name',
      Spanish: 'Donaldo',
      Catalan: 'No-Name',
      Portuguese: 'No-Name',
      Romanian: 'No-Name',
      
    }
  ],
  Donatus: [
    {
      German: 'Donatus',
      Dutch: 'Donaat',
      Maltese: 'Donat',
      Latin: 'Donatus',
      French: 'Donate',
      Italian: 'Donato',
      Spanish: 'Donato',
      Catalan: 'Donat',
      Portuguese: 'Donato',
      Romanian: 'No-Name',
      
    }
  ],
  Dorian: [
    {
      German: 'No-Name',
      Dutch: 'Dorian',
      Maltese: 'Dorjanu',
      Latin: 'Dorianus',
      French: 'No-Name',
      Italian: 'Doriano',
      Spanish: 'No-Name',
      Catalan: 'No-Name',
      Portuguese: 'No-Name',
      Romanian: 'Dorian',
      
    }
  ],
  Edgar: [
    {
      German: 'Edgar',
      Dutch: 'Edgar',
      Maltese: 'Edgardu',
      Latin: 'Edgar',
      French: 'Edgar',
      Italian: 'Edgardo',
      Spanish: 'Édgar, Edgardo',
      Catalan: 'No-Name',
      Portuguese: 'Edgar',
      Romanian: 'No-Name',
      
    }
  ],
  Edmund: [
    {
      German: 'Edmund',
      Dutch: 'Edmond, Edmund',
      Maltese: 'Edmundu',
      Latin: 'Edmundus',
      French: 'Edmond',
      Italian: 'Edmondo',
      Spanish: 'Edmundo',
      Catalan: 'No-Name',
      Portuguese: 'Edmundo',
      Romanian: 'No-Name',
      
    }
  ],
  Edward: [
    {
      German: 'Eduard',
      Dutch: 'Eduard',
      Maltese: 'Edwardu',
      Latin: 'Eduardus',
      French: 'Édouard',
      Italian: 'Edoardo',
      Spanish: 'Eduardo',
      Catalan: 'Eduard',
      Portuguese: 'Eduardo, Duarte',
      Romanian: 'Eduard',
      
    }
  ],
  Edwin: [
    {
      German: 'Edwin',
      Dutch: 'Edwin',
      Maltese: 'Edwinu',
      Latin: 'Edwinus, Edvinus',
      French: 'No-Name',
      Italian: 'Edvino',
      Spanish: 'Edvino',
      Catalan: 'No-Name',
      Portuguese: 'No-Name',
      Romanian: 'No-Name',
      
    }
  ],
  Ephraim: [
    {
      German: 'Ephraim',
      Dutch: 'Efraïm, Ephraïm',
      Maltese: 'Efrajm, Efrem',
      Latin: 'Ephraim',
      French: 'Ephrem',
      Italian: 'Efrem',
      Spanish: 'Efraín, Efrén',
      Catalan: 'No-Name',
      Portuguese: 'Efraim',
      Romanian: 'No-Name',
      
    }
  ],
  'Elias, Elija(h)': [
    {
      German: 'Elias',
      Dutch: 'Elias, Elia',
      Maltese: 'Elija',
      Latin: 'Elias',
      French: 'Élie',
      Italian: 'Elia',
      Spanish: 'Elías',
      Catalan: 'No-Name',
      Portuguese: 'Elias',
      Romanian: 'Ilie',
      
    }
  ],
  Eliezer: [
    {
      German: 'No-Name',
      Dutch: 'Eleazar, Eliëzer',
      Maltese: 'No-Name',
      Latin: 'Eleazar',
      French: 'Éléazar',
      Italian: 'Eleazaro',
      Spanish: 'Eliazar',
      Catalan: 'No-Name',
      Portuguese: 'Eleazar',
      Romanian: 'No-Name',
      
    }
  ],
  'Elliot(t)': [
    {
      German: 'No-Name',
      Dutch: 'No-Name',
      Maltese: 'No-Name',
      Latin: 'No-Name',
      French: 'No-Name',
      Italian: 'No-Name',
      Spanish: 'No-Name',
      Catalan: 'No-Name',
      Portuguese: 'No-Name',
      Romanian: 'No-Name',
      
    }
  ],
  Emile: [
    {
      German: 'Emil',
      Dutch: 'Emiel',
      Maltese: 'Emilju',
      Latin: 'Aemilius',
      French: 'Émile',
      Italian: 'Emilio',
      Spanish: 'Emilio',
      Catalan: 'Emili',
      Portuguese: 'Emílio',
      Romanian: 'Emil',
      
    }
  ],
  Emmanuel: [
    {
      German: 'Immanuel, Manuel',
      Dutch: 'Emmanuël',
      Maltese: 'Immanweli',
      Latin: 'Emmanuel',
      French: 'Emmanuel',
      Italian: 'Emanuele, Manuele',
      Spanish: 'Emanuel, Manuel',
      Catalan: 'Manel',
      Portuguese: 'Emanuel, Manuel, Manoel',
      Romanian: 'Emanoil',
      
    }
  ],
  Erastus: [
    {
      German: 'No-Name',
      Dutch: 'Erastus',
      Maltese: 'No-Name',
      Latin: 'No-Name',
      French: 'Euraste',
      Italian: 'Erasto',
      Spanish: 'Erasto',
      Catalan: 'No-Name',
      Portuguese: 'No-Name',
      Romanian: 'No-Name',
      
    }
  ],
  Ernest: [
    {
      German: 'Ernst',
      Dutch: 'Ernst',
      Maltese: 'Ernestu',
      Latin: 'Ernestus',
      French: 'Ernest',
      Italian: 'Ernesto',
      Spanish: 'Ernesto',
      Catalan: 'Ernest',
      Portuguese: 'Ernesto',
      Romanian: 'No-Name',
      
    }
  ],
  Erwin: [
    {
      German: 'Erwin',
      Dutch: 'Erwin',
      Maltese: 'No-Name',
      Latin: 'Erwinus',
      French: 'No-Name',
      Italian: 'Ervino',
      Spanish: 'No-Name',
      Catalan: 'No-Name',
      Portuguese: 'No-Name',
      Romanian: 'No-Name',
      
    }
  ],
  Eugene: [
    {
      German: 'Eugen',
      Dutch: 'Eugène',
      Maltese: 'Ewġenju',
      Latin: 'Eugenius',
      French: 'Eugène',
      Italian: 'Eugenio',
      Spanish: 'Eugenio',
      Catalan: 'Eugeni',
      Portuguese: 'Eugénio',
      Romanian: 'Eugen',
      
    }
  ],
  Eustratus: [
    {
      German: 'No-Name',
      Dutch: 'No-Name',
      Maltese: 'Ewstratu',
      Latin: 'Eustratus',
      French: 'Eustrate',
      Italian: 'Eustrato',
      Spanish: 'Eustrato',
      Catalan: 'No-Name',
      Portuguese: 'No-Name',
      Romanian: 'No-Name',
      
    }
  ],
  Fabian: [
    {
      German: 'Fabian',
      Dutch: 'Fabiaan',
      Maltese: 'Fabjanu',
      Latin: 'Fabianus',
      French: 'Fabien',
      Italian: 'Fabiano',
      Spanish: 'Fabián',
      Catalan: 'Fabià',
      Portuguese: 'Fábio',
      Romanian: 'Fabian',
      
    }
  ],
  Felix: [
    {
      German: 'Felix',
      Dutch: 'Felix',
      Maltese: 'Feliċ',
      Latin: 'Felix',
      French: 'Félix',
      Italian: 'Felice',
      Spanish: 'Félix',
      Catalan: 'Fèlix',
      Portuguese: 'Félix',
      Romanian: 'Felix',
      
    }
  ],
  Ferdinand: [
    {
      German: 'Ferdinand',
      Dutch: 'Ferdinand',
      Maltese: 'Ferdinandu',
      Latin: 'Ferdinand',
      French: 'Fernand',
      Italian: 'Fernando',
      Spanish: 'Fernando',
      Catalan: 'Ferran',
      Portuguese: 'Ferdinando, Fernando, Fernão',
      Romanian: 'No-Name',
      
    }
  ],
  Florian: [
    {
      German: 'Floriaan',
      Dutch: 'Florjanu',
      Maltese: 'Florianus',
      Latin: 'Florien , Florian',
      French: 'Floriano',
      Italian: 'Florián',
      Spanish: 'Florià',
      Catalan: 'Floriano',
      Portuguese: 'Florin',
      Romanian: 'No-Name',
      
    }
  ],
  'Francis, Frank': [
    {
      German: 'Franziskus, Franz, Frank',
      Dutch: 'Franġisku, Frans',
      Maltese: 'Franciscus, Frans, Frank',
      Latin: 'Franciscus',
      French: 'François, Frank',
      Italian: 'Francesco, Franco',
      Spanish: 'Francisco',
      Catalan: 'Francesc',
      Portuguese: 'Francisco',
      Romanian: 'Francisc, Ferenc',
      
    }
  ],
  'Frederic(k)': [
    {
      German: 'Friedrich, Fritz, Frederich',
      Dutch: 'Federiku',
      Maltese: 'Frederik, Freek',
      Latin: 'Fredericus',
      French: 'Frédéric',
      Italian: 'Federico',
      Spanish: 'Federico',
      Catalan: 'Frederic',
      Portuguese: 'Frederico',
      Romanian: 'No-Name',
      
    }
  ],
  Gabriel: [
    {
      German: 'Gabriel',
      Dutch: 'Gabriël',
      Maltese: 'Gabrijel, Grabiel',
      Latin: 'Gabriel',
      French: 'Gabriel',
      Italian: 'Gabriele',
      Spanish: 'Gabriel',
      Catalan: 'Gabriel',
      Portuguese: 'Gabriel',
      Romanian: 'Gavrilǎ, Gabriel',
      
    }
  ],
  Garrett: [
    {
      German: 'Gerhard',
      Dutch: 'Gerard, Gerrit',
      Maltese: 'No-Name',
      Latin: 'Gerardus',
      French: 'Gérard',
      Italian: 'Gerardo',
      Spanish: 'Gerardo',
      Catalan: 'Gerard',
      Portuguese: 'No-Name',
      Romanian: 'No-Name',
      
    }
  ],
  Gideon: [
    {
      German: 'Gideon',
      Dutch: 'Gideon',
      Maltese: 'No-Name',
      Latin: 'Gideon, Gedeon',
      French: 'Gédéon',
      Italian: 'Gedeone',
      Spanish: 'Gedeón',
      Catalan: 'No-Name',
      Portuguese: 'Gideão',
      Romanian: 'No-Name',
      
    }
  ],
  'Geoffrey, Godfrey, Jeffrey': [
    {
      German: 'Gottfried',
      Dutch: 'Godfried, Govert',
      Maltese: 'No-Name',
      Latin: 'Galfridus',
      French: 'Geoffroy',
      Italian: 'Goffredo',
      Spanish: 'Godofredo',
      Catalan: 'Godofred',
      Portuguese: 'Godofredo',
      Romanian: 'No-Name',
      
    }
  ],
  George: [
    {
      German: 'Georg, Jürgen',
      Dutch: 'George, Joris, Sjors, Jurriaan',
      Maltese: 'Ġorġ',
      Latin: 'Georgius',
      French: 'Georges',
      Italian: 'Giorgio',
      Spanish: 'Jorge',
      Catalan: 'Jordi',
      Portuguese: 'Jorge',
      Romanian: 'Gheorghe, George',
      
    }
  ],
  Gerald: [
    {
      German: 'Gerald',
      Dutch: 'Gerout, Gerold, Gerald',
      Maltese: 'Ġilardu',
      Latin: 'Geraldus',
      French: 'Gérald',
      Italian: 'Geraldo',
      Spanish: 'Geraldo',
      Catalan: 'No-Name',
      Portuguese: 'Geraldo',
      Romanian: 'No-Name',
      
    }
  ],
  'Gerard, Garrett': [
    {
      German: 'Gerhard, Gerd',
      Dutch: 'Gerard, Gerrit',
      Maltese: 'No-Name',
      Latin: 'Gerardus',
      French: 'Gérard, Girard',
      Italian: 'Gerardo',
      Spanish: 'Gerardo',
      Catalan: 'Gerard',
      Portuguese: 'Gerardo',
      Romanian: 'No-Name',
      
    }
  ],
  Gerasimus: [
    {
      German: 'No-Name',
      Dutch: 'Gerasimus',
      Maltese: 'No-Name',
      Latin: 'Gerasimus',
      French: 'No-Name',
      Italian: 'Gerasimo',
      Spanish: 'Gerásimo',
      Catalan: 'No-Name',
      Portuguese: 'Gherasim',
      Romanian: 'No-Name',
      
    }
  ],
  Gilbert: [
    {
      German: 'Gilbert',
      Dutch: 'Gilbert',
      Maltese: 'No-Name',
      Latin: 'Gilbertus',
      French: 'Gilbert',
      Italian: 'Gilberto',
      Spanish: 'Gilberto',
      Catalan: 'Gilbert',
      Portuguese: 'Gilberto',
      Romanian: 'No-Name',
      
    }
  ],
  Godwin: [
    {
      German: 'Gottwin',
      Dutch: 'Gozewijn',
      Maltese: 'No-Name',
      Latin: 'No-Name',
      French: 'No-Name',
      Italian: 'No-Name',
      Spanish: 'Godovino',
      Catalan: 'No-Name',
      Portuguese: 'No-Name',
      Romanian: 'No-Name',
      
    }
  ],
  Gonzalo: [
    {
      German: 'No-Name',
      Dutch: 'No-Name',
      Maltese: 'Gonsalvu',
      Latin: 'Gundisalvus',
      French: 'Gonzague',
      Italian: 'Consalvo',
      Spanish: 'Gonzalo',
      Catalan: 'Gonçal',
      Portuguese: 'Gonçalo',
      Romanian: 'No-Name',
      
    }
  ],
  Gregory: [
    {
      German: 'Gregor, Gregra',
      Dutch: 'Gregoor, Gregorius',
      Maltese: 'Gregorju',
      Latin: 'Gregorius',
      French: 'Grégoire',
      Italian: 'Gregorio',
      Spanish: 'Gregorio',
      Catalan: 'Gregori',
      Portuguese: 'Gregório',
      Romanian: 'Grigore',
      
    }
  ],
  Gunter: [
    {
      German: 'Gunter, Günter, Gunther, Günther',
      Dutch: 'Gunter, Gunther',
      Maltese: 'No-Name',
      Latin: 'Gunterus',
      French: 'No-Name',
      Italian: 'Gontiero',
      Spanish: 'No-Name',
      Catalan: 'No-Name',
      Portuguese: 'No-Name',
      Romanian: 'No-Name',
      
    }
  ],
  Guy: [
    {
      German: 'Guido',
      Dutch: 'Guido',
      Maltese: 'Gwidu',
      Latin: 'Guido',
      French: 'Guy',
      Italian: 'Guido',
      Spanish: 'Guido',
      Catalan: 'No-Name',
      Portuguese: 'Guido',
      Romanian: 'No-Name',
      
    }
  ],
  Gustave: [
    {
      German: 'Gustav',
      Dutch: 'Gustaaf',
      Maltese: 'No-Name',
      Latin: 'Gustavus',
      French: 'Gustave',
      Italian: 'Gustavo',
      Spanish: 'Gustavo',
      Catalan: 'Gustau',
      Portuguese: 'Gustavo',
      Romanian: 'No-Name',
      
    }
  ],
  Hank: [
    {
      German: 'Heinz',
      Dutch: 'Henk',
      Maltese: 'No-Name',
      Latin: 'No-Name',
      French: 'Riri',
      Italian: 'Enzo',
      Spanish: 'Enzo',
      Catalan: 'No-Name',
      Portuguese: 'No-Name',
      Romanian: 'No-Name',
      
    }
  ],
  Harold: [
    {
      German: 'Harald',
      Dutch: 'Harold',
      Maltese: 'No-Name',
      Latin: 'Haroldus',
      French: 'Harold',
      Italian: 'Aroldo',
      Spanish: 'No-Name',
      Catalan: 'No-Name',
      Portuguese: 'Haroldo',
      Romanian: 'No-Name',
      
    }
  ],
  'Henry, Harry': [
    {
      German: 'Heinrich',
      Dutch: 'Hendrik',
      Maltese: 'Neriku',
      Latin: 'Henricus',
      French: 'Henri',
      Italian: 'Enrico',
      Spanish: 'Enrique',
      Catalan: 'Enric',
      Portuguese: 'Henrique',
      Romanian: 'Horia',
      
    }
  ],
  Herbert: [
    {
      German: 'Herbert',
      Dutch: 'Herbert',
      Maltese: 'No-Name',
      Latin: 'Heribertus',
      French: 'Herbert',
      Italian: 'Eriberto, Erberto',
      Spanish: 'Heriberto',
      Catalan: 'Eribert',
      Portuguese: 'Heriberto',
      Romanian: 'No-Name',
      
    }
  ],
  Herman: [
    {
      German: 'Hermann',
      Dutch: 'Herman',
      Maltese: 'No-Name',
      Latin: 'Armandus, Hermanus',
      French: 'Armand',
      Italian: 'Ermanno',
      Spanish: 'Germán',
      Catalan: 'No-Name',
      Portuguese: 'Germano',
      Romanian: 'No-Name',
      
    }
  ],
  Hilary: [
    {
      German: 'Hilarius',
      Dutch: 'Hilarius',
      Maltese: 'Ilarju',
      Latin: 'Hilarius',
      French: 'Hilaire',
      Italian: 'Ilario',
      Spanish: 'Hilario',
      Catalan: 'Hilari',
      Portuguese: 'Hilário',
      Romanian: 'Ilarie',
      
    },
    {
      German: 'Hilarius',
      Dutch: 'Hilarius',
      Maltese: 'Ilarju',
      Latin: 'Hilarius',
      French: 'Hilaire',
      Italian: 'Ilarione',
      Spanish: 'Hilarión',
      Catalan: 'No-Name',
      Portuguese: 'Hilário',
      Romanian: 'Ilarion',
      
    }
  ],
  Hippolytus: [
    {
      German: 'No-Name',
      Dutch: 'Hippoliet',
      Maltese: 'Ippolitu',
      Latin: 'Hippolytus',
      French: 'Hippolyte',
      Italian: 'Ippolito',
      Spanish: 'Hipólito',
      Catalan: 'Hipòlit',
      Portuguese: 'Hipólito',
      Romanian: 'No-Name',
      
    }
  ],
  Horace: [
    {
      German: 'Horaz',
      Dutch: 'Horatius',
      Maltese: 'Orazju',
      Latin: 'Horatius',
      French: 'Horace',
      Italian: 'Orazio',
      Spanish: 'Horacio',
      Catalan: 'Horaci',
      Portuguese: 'Horácio',
      Romanian: 'Horațiu',
      
    }
  ],
  Hubert: [
    {
      German: 'Hubert',
      Dutch: 'Hubert, Huibrecht',
      Maltese: 'Ubertu',
      Latin: 'Hubertus',
      French: 'Hubert',
      Italian: 'Uberto',
      Spanish: 'Huberto',
      Catalan: 'No-Name',
      Portuguese: 'Huberto',
      Romanian: 'No-Name',
      
    }
  ],
  Hugh: [
    {
      German: 'Hugo',
      Dutch: 'Hugo, Huug',
      Maltese: 'Ugo',
      Latin: 'Hugonis',
      French: 'Hugues, Hugo',
      Italian: 'Ugo',
      Spanish: 'Hugo',
      Catalan: 'Hug',
      Portuguese: 'Hugo',
      Romanian: 'No-Name',
      
    }
  ],
  Ignatius: [
    {
      German: 'Ignaz',
      Dutch: 'Ignaat',
      Maltese: 'Injazju',
      Latin: 'Ignatius',
      French: 'Ignace',
      Italian: 'Ignazio',
      Spanish: 'Ignacio',
      Catalan: 'Ignasi',
      Portuguese: 'Inácio, Ignácio',
      Romanian: 'Ignat',
      
    }
  ],
  Igor: [
    {
      German: 'Igor',
      Dutch: 'Igor',
      Maltese: 'Igor',
      Latin: 'Igor',
      French: 'Igor',
      Italian: 'Igor',
      Spanish: 'Ígor',
      Catalan: 'Ígor',
      Portuguese: 'Igor',
      Romanian: 'No-Name',
      
    }
  ],
  Innocent: [
    {
      German: 'Innozenz',
      Dutch: 'Innocent',
      Maltese: 'Innoċenz',
      Latin: 'Innocentius',
      French: 'Innocent',
      Italian: 'Innocenzo',
      Spanish: 'Inocencio',
      Catalan: 'Inocenci',
      Portuguese: 'Inocêncio',
      Romanian: 'No-Name',
      
    }
  ],
  Isaac: [
    {
      German: 'Isaak',
      Dutch: 'Izaäk',
      Maltese: 'Iżakk',
      Latin: 'Isachus',
      French: 'Isaac',
      Italian: 'Isacco',
      Spanish: 'Isaac',
      Catalan: 'Isaac',
      Portuguese: 'Isaac',
      Romanian: 'Isac',
      
    }
  ],
  Isaiah: [
    {
      German: 'Isah',
      Dutch: 'Jesaja, Isaï',
      Maltese: 'Iżaija',
      Latin: 'Isaias, Esaias',
      French: 'Isaïe',
      Italian: 'Isaia',
      Spanish: 'Isaías',
      Catalan: 'No-Name',
      Portuguese: 'Isaías',
      Romanian: 'Isaia',
      
    }
  ],
  Isidore: [
    {
      German: 'Isidor',
      Dutch: 'Isidoor',
      Maltese: 'IŻidoru',
      Latin: 'Isidorus',
      French: 'Isidore',
      Italian: 'Isidoro',
      Spanish: 'Isidoro, Isidro',
      Catalan: 'Isidor, Isidre',
      Portuguese: 'Isidoro',
      Romanian: 'Izidor',
      
    }
  ],
  'James, Jacob, Jake, Séamus': [
    {
      German: 'Jakob',
      Dutch: 'Jakob, Jacob, Jaap',
      Maltese: 'Ġakbu, Ġakobb',
      Latin: 'Iacobus, Iacomus',
      French: 'Jacques, Jacob',
      Italian: 'Giacomo, Giacobbe, Iacopo',
      Spanish: 'Jaime, Jacobo, Diego, Santiago, Yago',
      Catalan: 'Jaume, Dídac',
      Portuguese: 'Jaime, Diogo, Jacob, Jacó, Tiago, Thiago',
      Romanian: 'Iacob',
      
    }
  ],
  'Jeremy, Jeremiah, Jerry': [
    {
      German: 'Jeremiah',
      Dutch: 'Jeremias, Jeremia',
      Maltese: 'Ġeremija',
      Latin: 'Ieremias',
      French: 'Jérémy, Jérémie',
      Italian: 'Geremia',
      Spanish: 'Jeremías',
      Catalan: 'No-Name',
      Portuguese: 'Jeremias',
      Romanian: 'Eremia',
      
    }
  ],
  Jerome: [
    {
      German: 'Hieronymus',
      Dutch: 'Jeroen',
      Maltese: 'Ġlormu',
      Latin: 'Hieronymus',
      French: 'Jérôme',
      Italian: 'Geronimo',
      Spanish: 'Jerónimo, Gerónimo',
      Catalan: 'Jeroni',
      Portuguese: 'Jerónimo',
      Romanian: 'Eremia',
      
    }
  ],
  Joachim: [
    {
      German: 'Joachim, Achim, Jochen',
      Dutch: 'Joachim, Jochem',
      Maltese: 'Ġwakkin',
      Latin: 'Ioachim',
      French: 'Joachim',
      Italian: 'Gioacchino',
      Spanish: 'Joaquín',
      Catalan: 'Joaquim',
      Portuguese: 'Joaquim',
      Romanian: 'Ichim',
      
    }
  ],
  'John, Ian, Sean, Evan, Ewan': [
    {
      German: 'Johann, Johannes, Johanns, Jan, Hans, Jens',
      Dutch: 'Jan, Johannes, Hans',
      Maltese: 'Ġwanni, Ġanni, Ġovann',
      Latin: 'Ioannes',
      French: 'Jean, Yann',
      Italian: 'Giovanni, Gianni',
      Spanish: 'Juan',
      Catalan: 'Joan',
      Portuguese: 'João',
      Romanian: 'Ion, Ioan',
      
    }
  ],
  Jonathan: [
    {
      German: 'Jonatan, Jönten',
      Dutch: 'Jonathan',
      Maltese: 'Ġonatan',
      Latin: 'Ionathan',
      French: 'Jonathan',
      Italian: 'Gionata',
      Spanish: 'Jonathan',
      Catalan: 'No-Name',
      Portuguese: 'Jónatas',
      Romanian: 'Ionathan',
      
    }
  ],
  Joseph: [
    {
      German: 'Josef',
      Dutch: 'Jozef, Josef, Joop',
      Maltese: 'Ġużeppi',
      Latin: 'Iosephus',
      French: 'Joseph',
      Italian: 'Giuseppe',
      Spanish: 'José',
      Catalan: 'Josep',
      Portuguese: 'José',
      Romanian: 'Iosif',
      
    }
  ],
  Joshua: [
    {
      German: 'Josua',
      Dutch: 'Jozue',
      Maltese: 'Ġożwe',
      Latin: 'Josue',
      French: 'Josué',
      Italian: 'Giosué',
      Spanish: 'Josué',
      Catalan: 'No-Name',
      Portuguese: 'Josué',
      Romanian: 'No-Name',
      
    }
  ],
  Julian: [
    {
      German: 'Julian, Juli',
      Dutch: 'Juliaan',
      Maltese: 'Ġiljan',
      Latin: 'Iulianus',
      French: 'Julien',
      Italian: 'Giuliano',
      Spanish: 'Julián',
      Catalan: 'Julià',
      Portuguese: 'Juliano, Julião',
      Romanian: 'Iulian',
      
    }
  ],
  Julius: [
    {
      German: 'Julius, Juli',
      Dutch: 'Julius, Jules',
      Maltese: 'Ġulju',
      Latin: 'Iulius',
      French: 'Jules',
      Italian: 'Giulio',
      Spanish: 'Julio',
      Catalan: 'Juli',
      Portuguese: 'Júlio',
      Romanian: 'Julius, Iuliu',
      
    }
  ],
  Justin: [
    {
      German: 'Justin',
      Dutch: 'Justin',
      Maltese: 'Ġustinu',
      Latin: 'Iustinus',
      French: 'Justin',
      Italian: 'Giustino',
      Spanish: 'Justino',
      Catalan: 'Justí',
      Portuguese: 'Justino',
      Romanian: 'Iustin, Iustinian',
      
    }
  ],
  Calistratus: [
    {
      German: 'No-Name',
      Dutch: 'No-Name',
      Maltese: 'No-Name',
      Latin: 'Calistratus',
      French: 'No-Name',
      Italian: 'Callistrato',
      Spanish: 'Calístrato',
      Catalan: 'No-Name',
      Portuguese: 'Calístrato',
      Romanian: 'Calistrat',
      
    }
  ],
  'Kevin, Caoimhín': [
    {
      German: 'Kevin',
      Dutch: 'Kévin',
      Maltese: 'Kevin',
      Latin: 'Coemgenus',
      French: 'No-Name',
      Italian: 'No-Name',
      Spanish: 'No-Name',
      Catalan: 'No-Name',
      Portuguese: 'No-Name',
      Romanian: 'No-Name',
      
    }
  ],
  Lawrence: [
    {
      German: 'Lorenz, Laurenz, Laürnt',
      Dutch: 'Laurens',
      Maltese: 'Lawrenz',
      Latin: 'Laurentius',
      French: 'Laurent',
      Italian: 'Lorenzo',
      Spanish: 'Lorenzo',
      Catalan: 'Llorenç',
      Portuguese: 'Lourenço',
      Romanian: 'Laurențiu',
      
    }
  ],
  Lazarus: [
    {
      German: 'Lazar, Lazarus',
      Dutch: 'Lazarus',
      Maltese: 'Lazzru',
      Latin: 'Lazarus',
      French: 'Lazare',
      Italian: 'Lazzaro',
      Spanish: 'Lázaro',
      Catalan: 'No-Name',
      Portuguese: 'Lázaro',
      Romanian: 'Lazǎr',
      
    }
  ],
  'Leo, Leon': [
    {
      German: 'Leon',
      Dutch: 'Leo',
      Maltese: 'Ljun',
      Latin: 'Leo',
      French: 'Léo(n)',
      Italian: 'Leo, Leone',
      Spanish: 'León',
      Catalan: 'Lleó',
      Portuguese: 'Leão',
      Romanian: 'Leon',
      
    }
  ],
  Leonard: [
    {
      German: 'Leonhard',
      Dutch: 'Leonard',
      Maltese: 'Leonardu',
      Latin: 'Leonardus',
      French: 'Léonard',
      Italian: 'Leonardo',
      Spanish: 'Leonardo',
      Catalan: 'No-Name',
      Portuguese: 'Leonardo',
      Romanian: 'No-Name',
      
    }
  ],
  Leonide: [
    {
      German: 'Leonid',
      Dutch: 'Leonidas',
      Maltese: 'Leonidas',
      Latin: 'Leonidas',
      French: 'Leonide',
      Italian: 'Leonida',
      Spanish: 'Leónidas',
      Catalan: 'No-Name',
      Portuguese: 'Leónidas',
      Romanian: 'Leonida',
      
    }
  ],
  Leopold: [
    {
      German: 'Leopold, Luitpold',
      Dutch: 'Leopold',
      Maltese: 'Leopoldu',
      Latin: 'Leopoldus',
      French: 'Léopold',
      Italian: 'Leopoldo',
      Spanish: 'Leopoldo',
      Catalan: 'Leopoldo',
      Portuguese: 'No-Name',
      Romanian: 'No-Name',
      
    }
  ],
  Lionel: [
    {
      German: 'Leonel, Lönel',
      Dutch: 'Lionel',
      Maltese: 'Ljunell',
      Latin: 'Lionel',
      French: 'Lionello',
      Italian: 'Lionel',
      Spanish: 'No-Name',
      Catalan: 'Leonel',
      Portuguese: 'No-Name',
      Romanian: 'No-Name',
      
    }
  ],
  'Lewis, Louis': [
    {
      German: 'Ludwig',
      Dutch: 'Lodewijk',
      Maltese: 'Ludoviku, Alwiġi',
      Latin: 'Ludovicus, Aloysius',
      French: 'Louis',
      Italian: 'Luigi',
      Spanish: 'Luis',
      Catalan: 'Lluís',
      Portuguese: 'Luís',
      Romanian: 'Ludovic',
      
    }
  ],
  'Luke, Lucas': [
    {
      German: 'Lukas, Lucas',
      Dutch: 'Lukas, Lucas',
      Maltese: 'Luqa',
      Latin: 'Lucas',
      French: 'Luc, Lucas',
      Italian: 'Luca',
      Spanish: 'Lucas',
      Catalan: 'Lluc',
      Portuguese: 'Lucas',
      Romanian: 'Luca',
      
    }
  ],
  Lucianus: [
    {
      German: 'Luzian, Lutzen',
      Dutch: 'Lucien',
      Maltese: 'Luċjan',
      Latin: 'Lucianus',
      French: 'Lucien',
      Italian: 'Luciano',
      Spanish: 'Luciano',
      Catalan: 'No-Name',
      Portuguese: 'Luciano',
      Romanian: 'Lucian',
      
    }
  ],
  Magnus: [
    {
      German: 'Magnus',
      Dutch: 'Magnus',
      Maltese: 'Manju',
      Latin: 'Magnus',
      French: 'No-Name',
      Italian: 'Magno',
      Spanish: 'Magno',
      Catalan: 'No-Name',
      Portuguese: 'Magno',
      Romanian: 'No-Name',
      
    }
  ],
  Macarius: [
    {
      German: 'Makarius',
      Dutch: 'Macarius',
      Maltese: 'Makarju',
      Latin: 'Macarius',
      French: 'Macaire',
      Italian: 'Macario',
      Spanish: 'Macario',
      Catalan: 'No-Name',
      Portuguese: 'Macário',
      Romanian: 'Macarie',
      
    }
  ],
  'Mark, Marcus': [
    {
      German: 'Markus',
      Dutch: 'Mark, Marc, Marcus',
      Maltese: 'Mark, Marku',
      Latin: 'Marcus',
      French: 'Marc',
      Italian: 'Marco',
      Spanish: 'Marcos, Marco',
      Catalan: 'Marc',
      Portuguese: 'Marcos, Marco',
      Romanian: 'Marcu',
      
    }
  ],
  Martin: [
    {
      German: 'Martin',
      Dutch: 'Maarten, Martijn, Martin',
      Maltese: 'Martin',
      Latin: 'Martinus',
      French: 'Martin',
      Italian: 'Martino',
      Spanish: 'Martín',
      Catalan: 'Martí',
      Portuguese: 'Martim, Martinho',
      Romanian: 'Martin',
      
    }
  ],
  Matthew: [
    {
      German: 'Matthäus',
      Dutch: 'Mattheüs, Matthijs',
      Maltese: 'Mattew',
      Latin: 'Matthaeus',
      French: 'Matthieu',
      Italian: 'Matteo',
      Spanish: 'Mateo',
      Catalan: 'Mateu, Mateus',
      Portuguese: 'Matheus, Mateus',
      Romanian: 'No-Name',
      
    }
  ],
  Matthias: [
    {
      German: 'Matthias',
      Dutch: 'Matthias',
      Maltese: 'Mattija',
      Latin: 'Matthaeus',
      French: 'Matthias',
      Italian: 'Mattia',
      Spanish: 'Matías',
      Catalan: 'No-Name',
      Portuguese: 'Mathias, Matias',
      Romanian: 'Matei',
      
    }
  ],
  'Maurice, Morris': [
    {
      German: 'Moritz',
      Dutch: 'Maurits, Maup',
      Maltese: 'Mawrizju',
      Latin: 'Mauritius',
      French: 'Maurice',
      Italian: 'Maurizio',
      Spanish: 'Mauricio',
      Catalan: 'Maurici',
      Portuguese: 'Maurício',
      Romanian: 'Marius',
      
    }
  ],
  'No-Name': [
    {
      German: 'Maxim',
      Dutch: 'Maxime',
      Maltese: 'Massimu',
      Latin: 'Maximus',
      French: 'Maxime',
      Italian: 'Massimo',
      Spanish: 'Máximo',
      Catalan: 'Maxim',
      Portuguese: 'No-Name',
      Romanian: 'Máximo',
      
    },
    {
      German: 'Seraphim, Serafin',
      Dutch: 'Serafin',
      Maltese: 'No-Name',
      Latin: 'Seraphinus',
      French: 'Séraphin',
      Italian: 'Serafino',
      Spanish: 'Serafín',
      Catalan: 'Serafí',
      Portuguese: 'Serafim',
      Romanian: 'Serafim',
      
    }
  ],
  'Maximilian, Max, Maxim': [
    {
      German: 'Maximilian, Max',
      Dutch: 'Maximiliaan',
      Maltese: 'Massimiljanu',
      Latin: 'Maximilianus',
      French: 'Maximilien',
      Italian: 'Massimiliano',
      Spanish: 'Maximiliano',
      Catalan: 'No-Name',
      Portuguese: 'Maximilian',
      Romanian: 'Maximilian',
      
    }
  ],
  Michael: [
    {
      German: 'Michael',
      Dutch: 'Michaël, Michiel',
      Maltese: 'Mikiel',
      Latin: 'Michael',
      French: 'Michel',
      Italian: 'Michele',
      Spanish: 'Miguel',
      Catalan: 'Miquel',
      Portuguese: 'Miguel',
      Romanian: 'Mihail, Mihai',
      
    }
  ],
  Myron: [
    {
      German: 'No-Name',
      Dutch: 'Myron',
      Maltese: 'No-Name',
      Latin: 'Myron',
      French: 'No-Name',
      Italian: 'Mirone',
      Spanish: 'Mirón',
      Catalan: 'No-Name',
      Portuguese: 'Miron',
      Romanian: 'Miron',
      
    }
  ],
  Modestus: [
    {
      German: 'No-Name',
      Dutch: 'Modestus',
      Maltese: 'Mudest',
      Latin: 'Modestus',
      French: 'Modeste',
      Italian: 'Modesto',
      Spanish: 'Modesto',
      Catalan: 'Modest',
      Portuguese: 'Modesto',
      Romanian: 'No-Name',
      
    }
  ],
  Moses: [
    {
      German: 'Mose(s)',
      Dutch: 'Mozes, Moses, Moos',
      Maltese: 'Mose',
      Latin: 'Moises',
      French: 'Moïse',
      Italian: 'Mosé',
      Spanish: 'Moisés',
      Catalan: 'No-Name',
      Portuguese: 'Moisés',
      Romanian: 'Moise',
      
    }
  ],
  Nathan: [
    {
      German: 'Nathan',
      Dutch: 'Nathan',
      Maltese: 'Natan',
      Latin: 'Nathan',
      French: 'Nathan',
      Italian: 'No-Name',
      Spanish: 'No-Name',
      Catalan: 'No-Name',
      Portuguese: 'Natán',
      Romanian: 'No-Name',
      
    }
  ],
  Nathaniel: [
    {
      German: 'No-Name',
      Dutch: 'Nathanaël',
      Maltese: 'Natanjel',
      Latin: 'Nathaniel',
      French: 'Nathaniel',
      Italian: 'Nataniele',
      Spanish: 'Nataniel',
      Catalan: 'No-Name',
      Portuguese: 'Nataniel',
      Romanian: 'Natanael',
      
    }
  ],
  Nazarius: [
    {
      German: 'No-Name',
      Dutch: 'Nazarius',
      Maltese: 'Nażarju',
      Latin: 'Nazarius',
      French: 'Nazaire',
      Italian: 'Nazario',
      Spanish: 'Nazario',
      Catalan: 'No-Name',
      Portuguese: 'Nazário',
      Romanian: 'Nazarie',
      
    }
  ],
  Nestor: [
    {
      German: 'Nestor',
      Dutch: 'Nestor',
      Maltese: 'Nestor',
      Latin: 'Nestor',
      French: 'Nestor',
      Italian: 'Nestore',
      Spanish: 'Néstor',
      Catalan: 'No-Name',
      Portuguese: 'Nestor',
      Romanian: 'Nestor',
      
    }
  ],
  Nicholas: [
    {
      German: 'Nikolaus, Klaus, Nicklaus',
      Dutch: 'Nicolaas, Nikolaas, Niklaas',
      Maltese: 'Nikola',
      Latin: 'Nicolaus',
      French: 'Nicolas',
      Italian: 'Nicola, Niccolò',
      Spanish: 'Nicolás',
      Catalan: 'Nicolau',
      Portuguese: 'Nicolau',
      Romanian: 'Nicolae',
      
    }
  ],
  Nicodemus: [
    {
      German: 'Nikodemus',
      Dutch: 'Nicodemus',
      Maltese: 'Nikodemu',
      Latin: 'Nicodemus',
      French: 'Nicodème',
      Italian: 'Nicodemo',
      Spanish: 'Nicodemo',
      Catalan: 'No-Name',
      Portuguese: 'Nicodemo',
      Romanian: 'Nicodim',
      
    }
  ],
  Octavius: [
    {
      German: 'Octavius',
      Dutch: 'Octaaf',
      Maltese: 'Ottavju, Ottavjanu',
      Latin: 'Octavius, Octavianus',
      French: 'Octave',
      Italian: 'Ottavio, Ottaviano',
      Spanish: 'Octavio',
      Catalan: 'Octavi',
      Portuguese: 'No-Name',
      Romanian: 'Octavian',
      
    }
  ],
  Olaf: [
    {
      German: 'Olaf',
      Dutch: 'Olaf',
      Maltese: 'Olaf',
      Latin: 'Olaus, Olavus',
      French: 'Olaf',
      Italian: 'Olao',
      Spanish: 'No-Name',
      Catalan: 'No-Name',
      Portuguese: 'Olavo',
      Romanian: 'No-Name',
      
    }
  ],
  Olgierdus: [
    {
      German: 'Olgierd, Oleg',
      Dutch: 'Oleg',
      Maltese: 'No-Name',
      Latin: 'Olgierdus',
      French: 'Oleg',
      Italian: 'Olegario, Olgierdo',
      Spanish: 'Olegario',
      Catalan: 'Oleguer',
      Portuguese: 'Olegário',
      Romanian: 'Oleg',
      
    }
  ],
  Oliver: [
    {
      German: 'Oliver',
      Dutch: 'Olivier',
      Maltese: 'Ulivier',
      Latin: 'Oliverius',
      French: 'Olivier',
      Italian: 'Oliviero, Oliverio',
      Spanish: 'Olivio',
      Catalan: 'No-Name',
      Portuguese: 'No-Name',
      Romanian: 'No-Name',
      
    }
  ],
  Onuphrius: [
    {
      German: 'No-Name',
      Dutch: 'No-Name',
      Maltese: 'Onofriju',
      Latin: 'Onuphrius',
      French: 'No-Name',
      Italian: 'Onofrio',
      Spanish: 'Onofre',
      Catalan: 'No-Name',
      Portuguese: 'Onofre',
      Romanian: 'Onofrei',
      
    }
  ],
  Orestes: [
    {
      German: 'No-Name',
      Dutch: 'No-Name',
      Maltese: 'Oreste',
      Latin: 'Orestes',
      French: 'Oreste',
      Italian: 'Oreste',
      Spanish: 'No-Name',
      Catalan: 'Orestes',
      Portuguese: 'No-Name',
      Romanian: 'Oreste',
      
    }
  ],
  Oscar: [
    {
      German: 'Oskar',
      Dutch: 'Oscar, Oskar',
      Maltese: 'Oskar',
      Latin: 'Anscharius',
      French: 'Oscar',
      Italian: 'Oscar',
      Spanish: 'Óscar',
      Catalan: 'No-Name',
      Portuguese: 'Óscar',
      Romanian: 'Oscar',
      
    }
  ],
  'Osmund, Osmond': [
    {
      German: 'Osmond',
      Dutch: 'Ożmundu',
      Maltese: 'Osmund',
      Latin: 'No-Name',
      French: 'Osmond',
      Italian: 'Osmondo',
      Spanish: 'Osmundo',
      Catalan: 'No-Name',
      Portuguese: 'No-Name',
      Romanian: 'No-Name',
      
    }
  ],
  Pancras: [
    {
      German: 'Pankraz',
      Dutch: 'Pancras',
      Maltese: 'Pankrazju',
      Latin: 'Pancratius',
      French: 'Pancrace',
      Italian: 'Pancrazio',
      Spanish: 'Pancracio',
      Catalan: 'Pancraci',
      Portuguese: 'Pancrácio',
      Romanian: 'No-Name',
      
    }
  ],
  Pantaleon: [
    {
      German: 'No-Name',
      Dutch: 'Pantaleon',
      Maltese: 'Pantaljun',
      Latin: 'Pantaleon',
      French: 'Pantaléon',
      Italian: 'Pantaleone',
      Spanish: 'Pantaleón',
      Catalan: 'No-Name',
      Portuguese: 'Pantaleão',
      Romanian: 'No-Name',
      
    }
  ],
  Patrick: [
    {
      German: 'Patrick',
      Dutch: 'Patrick, Patrik',
      Maltese: 'Patrizju',
      Latin: 'Patricius',
      French: 'Patrice, Patrick',
      Italian: 'Patrizio',
      Spanish: 'Patricio',
      Catalan: 'No-Name',
      Portuguese: 'Patrício',
      Romanian: 'No-Name',
      
    }
  ],
  Paul: [
    {
      German: 'Paul',
      Dutch: 'Paul',
      Maltese: 'Pawlu',
      Latin: 'Paulus',
      French: 'Paul',
      Italian: 'Paolo',
      Spanish: 'Pablo',
      Catalan: 'Pau',
      Portuguese: 'Paulo',
      Romanian: 'Pavel, Paul',
      
    }
  ],
  Peter: [
    {
      German: 'Peter',
      Dutch: 'Pieter, Peter, Piet',
      Maltese: 'Pietru',
      Latin: 'Petrus',
      French: 'Pierre',
      Italian: 'Pietro',
      Spanish: 'Pedro',
      Catalan: 'Pere',
      Portuguese: 'Pedro',
      Romanian: 'Petre, Petru',
      
    }
  ],
  Philemon: [
    {
      German: 'No-Name',
      Dutch: 'Filemon',
      Maltese: 'Filomenu',
      Latin: 'Philemon',
      French: 'Philémon',
      Italian: 'Filemone',
      Spanish: 'Filemón',
      Catalan: 'No-Name',
      Portuguese: 'No-Name',
      Romanian: 'Filimon',
      
    }
  ],
  'Phil(l)ip': [
    {
      German: 'Philipp',
      Dutch: 'Filip, Filips, Philips',
      Maltese: 'Filippu',
      Latin: 'Philippus',
      French: 'Philippe',
      Italian: 'Filippo',
      Spanish: 'Felipe',
      Catalan: 'No-Name',
      Portuguese: 'Filipe',
      Romanian: 'Filip',
      
    }
  ],
  Plato: [
    {
      German: 'Plato',
      Dutch: 'Plato',
      Maltese: 'Platun',
      Latin: 'Plato',
      French: 'Platon',
      Italian: 'Platone',
      Spanish: 'Platón',
      Catalan: 'Plató',
      Portuguese: 'Platão',
      Romanian: 'Platon',
      
    }
  ],
  Polycarpus: [
    {
      German: 'No-Name',
      Dutch: 'Polycarpus',
      Maltese: 'Polikarpu',
      Latin: 'Polycarpus',
      French: 'Polycarpe',
      Italian: 'Policarpo',
      Spanish: 'Policarpo',
      Catalan: 'No-Name',
      Portuguese: 'Policarpo',
      Romanian: 'No-Name',
      
    }
  ],
  Porphyrius: [
    {
      German: 'No-Name',
      Dutch: 'Porfyrius',
      Maltese: 'Porfirju',
      Latin: 'Porphyrius',
      French: 'Porphyre',
      Italian: 'Porfirio',
      Spanish: 'Porfirio',
      Catalan: 'No-Name',
      Portuguese: 'Porfírio',
      Romanian: 'No-Name',
      
    }
  ],
  Proclus: [
    {
      German: 'No-Name',
      Dutch: 'No-Name',
      Maltese: 'Proklu',
      Latin: 'Proclus',
      French: 'No-Name',
      Italian: 'Proclo',
      Spanish: 'Próculo',
      Catalan: 'No-Name',
      Portuguese: 'No-Name',
      Romanian: 'No-Name',
      
    }
  ],
  Procopius: [
    {
      German: 'Prokopius',
      Dutch: 'Procopius',
      Maltese: 'Prokopju',
      Latin: 'Procopius',
      French: 'Procope',
      Italian: 'Procopio',
      Spanish: 'Procopio',
      Catalan: 'No-Name',
      Portuguese: 'Procópio',
      Romanian: 'Procopie',
      
    }
  ],
  'Quentin, Quinton': [
    {
      German: 'Quentin',
      Dutch: 'Quinten',
      Maltese: 'Kwintinu',
      Latin: 'Quintinus',
      French: 'Quentin',
      Italian: 'Quintino',
      Spanish: 'Quintín',
      Catalan: 'No-Name',
      Portuguese: 'Quintino',
      Romanian: 'No-Name',
      
    }
  ],
  'Randolph, Ralph': [
    {
      German: 'Randolf, Randulf, Ralf, Ralph',
      Dutch: 'Randolf',
      Maltese: 'Ralf, Randolfu',
      Latin: 'Randolphus',
      French: 'Raoul, Ralph',
      Italian: 'Randolfo',
      Spanish: 'Randolfo, Raúl',
      Catalan: 'No-Name',
      Portuguese: 'Randolfo, Raul',
      Romanian: 'No-Name',
      
    }
  ],
  Raphael: [
    {
      German: 'Raphael',
      Dutch: 'Rafaël, Raphaël',
      Maltese: 'Raflu, Rafel',
      Latin: 'Raphaël',
      French: 'Raphaël',
      Italian: 'Raffaele, Raffaello',
      Spanish: 'Rafael',
      Catalan: 'Rafel',
      Portuguese: 'Rafael',
      Romanian: 'No-Name',
      
    }
  ],
  Raymond: [
    {
      German: 'Raimund',
      Dutch: 'Raimond, Raymond',
      Maltese: 'Rajmundu, Rajmondu',
      Latin: 'Raimundus',
      French: 'Raymond',
      Italian: 'Raimondo',
      Spanish: 'Ramón, Raimundo',
      Catalan: 'Raimon',
      Portuguese: 'Raimundo',
      Romanian: 'No-Name',
      
    }
  ],
  Reginald: [
    {
      German: 'Reginald',
      Dutch: 'Reginald',
      Maltese: 'Reġinaldu',
      Latin: 'Reginaldus',
      French: 'Réginald',
      Italian: 'Reginaldo',
      Spanish: 'Reginaldo',
      Catalan: 'No-Name',
      Portuguese: 'Reginaldo',
      Romanian: 'No-Name',
      
    }
  ],
  Reynold: [
    {
      German: 'Reinhold',
      Dutch: 'Reinout',
      Maltese: 'Rejnaldu',
      Latin: 'No-Name',
      French: 'Renaud',
      Italian: 'Rinaldo',
      Spanish: 'Reinaldo',
      Catalan: 'No-Name',
      Portuguese: 'Reinaldo',
      Romanian: 'No-Name',
      
    }
  ],
  Richard: [
    {
      German: 'Richard',
      Dutch: 'Richard, Rikkert',
      Maltese: 'Rikkardu',
      Latin: 'Ricardus',
      French: 'Richard',
      Italian: 'Riccardo',
      Spanish: 'Ricardo',
      Catalan: 'Ricard',
      Portuguese: 'Ricardo',
      Romanian: 'No-Name',
      
    }
  ],
  'Robert, Rupert': [
    {
      German: 'Robert, Robrecht, Rupert, Ruprecht',
      Dutch: 'Robert, Robrecht, Rupert',
      Maltese: 'No-Name',
      Latin: 'Robertus, Rupertus',
      French: 'Robert',
      Italian: 'Roberto',
      Spanish: 'Roberto',
      Catalan: 'Robert',
      Portuguese: 'Robert',
      Romanian: 'No-Name',
      
    }
  ],
  Roderick: [
    {
      German: 'Roderich',
      Dutch: 'Roderik',
      Maltese: 'No-Name',
      Latin: 'Rodericus',
      French: 'Rodrigue',
      Italian: 'Rodrigo',
      Spanish: 'Rodrigo',
      Catalan: 'No-Name',
      Portuguese: 'Rodrigo',
      Romanian: 'No-Name',
      
    }
  ],
  Roger: [
    {
      German: 'Rüdiger',
      Dutch: 'Rutger, Rogier',
      Maltese: 'No-Name',
      Latin: 'Rogerius',
      French: 'Roger',
      Italian: 'Ruggero',
      Spanish: 'Rogelio',
      Catalan: 'Roger',
      Portuguese: 'Rogério',
      Romanian: 'No-Name',
      
    }
  ],
  Roland: [
    {
      German: 'Roland',
      Dutch: 'Roland',
      Maltese: 'No-Name',
      Latin: 'Rolandus',
      French: 'Roland',
      Italian: 'Rolando',
      Spanish: 'Rolando, Roldán',
      Catalan: 'No-Name',
      Portuguese: 'Rolando',
      Romanian: 'Roland',
      
    }
  ],
  Roman: [
    {
      German: 'Roman',
      Dutch: 'Romanus',
      Maltese: 'No-Name',
      Latin: 'Romanus',
      French: 'Romain',
      Italian: 'Romano',
      Spanish: 'Román',
      Catalan: 'No-Name',
      Portuguese: 'Romão',
      Romanian: 'Roman',
      
    }
  ],
  Romeo: [
    {
      German: 'Romeo',
      Dutch: 'Romeo',
      Maltese: 'No-Name',
      Latin: 'Romeus',
      French: 'Roméo',
      Italian: 'Romeo',
      Spanish: 'Romeo',
      Catalan: 'Romeu',
      Portuguese: 'Romeu',
      Romanian: 'No-Name',
      
    }
  ],
  Ronald: [
    {
      German: 'Ronald',
      Dutch: 'Ronald',
      Maltese: 'No-Name',
      Latin: 'Ronaldus',
      French: 'Ronald',
      Italian: 'Ronaldo',
      Spanish: 'Ronaldo',
      Catalan: 'No-Name',
      Portuguese: 'Ronaldo',
      Romanian: 'No-Name',
      
    }
  ],
  Rudolph: [
    {
      German: 'Rudolf, Rudi, Rolf',
      Dutch: 'Rudolf, Ruud',
      Maltese: 'No-Name',
      Latin: 'Rudolphus',
      French: 'Rodolphe',
      Italian: 'Rodolfo',
      Spanish: 'Rodolfo',
      Catalan: 'No-Name',
      Portuguese: 'Rodolfo',
      Romanian: 'No-Name',
      
    }
  ],
  Samson: [
    {
      German: 'Samson',
      Dutch: 'Samson, Simson',
      Maltese: 'No-Name',
      Latin: 'Samson',
      French: 'Samson',
      Italian: 'Sansone',
      Spanish: 'Sansón',
      Catalan: 'No-Name',
      Portuguese: 'Sansão',
      Romanian: 'Samson',
      
    }
  ],
  Samuel: [
    {
      German: 'Samuel',
      Dutch: 'Samuël',
      Maltese: 'Samuel',
      Latin: 'Samuel',
      French: 'Samuel',
      Italian: 'Samuele',
      Spanish: 'Samuel',
      Catalan: 'No-Name',
      Portuguese: 'Samuel',
      Romanian: 'Samuil',
      
    }
  ],
  Salvator: [
    {
      German: 'No-Name',
      Dutch: 'No-Name',
      Maltese: 'No-Name',
      Latin: 'Salvator',
      French: 'Sauveur',
      Italian: 'Salvatore',
      Spanish: 'Salvador',
      Catalan: 'Salvador',
      Portuguese: 'Salvador',
      Romanian: 'No-Name',
      
    }
  ],
  'Sebastian, Bastian': [
    {
      German: 'Sebastian, Bastian',
      Dutch: 'Sebastiaan, Bastiaan',
      Maltese: 'No-Name',
      Latin: 'Sebastianus',
      French: 'Sébastien, Bastien',
      Italian: 'Sebastiano',
      Spanish: 'Sebastián',
      Catalan: 'Sebastià',
      Portuguese: 'Sebastião',
      Romanian: 'Sebastian',
      
    }
  ],
  Serge: [
    {
      German: 'Serge, Sergius',
      Dutch: 'Serge',
      Maltese: 'No-Name',
      Latin: 'Sergius',
      French: 'Serge',
      Italian: 'Sergio',
      Spanish: 'Sergio',
      Catalan: 'Sergi',
      Portuguese: 'Sérgio',
      Romanian: 'Sergiu',
      
    }
  ],
  Severin: [
    {
      German: 'Severin, Sebern, Severn',
      Dutch: 'Severijn',
      Maltese: 'No-Name',
      Latin: 'Severinus',
      French: 'Sévère, Séverin',
      Italian: 'Severo, Severino',
      Spanish: 'Severo, Severino',
      Catalan: 'Severí',
      Portuguese: 'Severo, Severino',
      Romanian: 'Severin',
      
    }
  ],
  Sigmund: [
    {
      German: 'Siegmund, Sigismund',
      Dutch: 'Siegmund, Sigismund',
      Maltese: 'No-Name',
      Latin: 'Sigismundus',
      French: 'Sigismond',
      Italian: 'Sigismondo',
      Spanish: 'Segismundo',
      Catalan: 'Sigismundo',
      Portuguese: 'No-Name',
      Romanian: 'Sigismund',
      
    }
  ],
  Sylvester: [
    {
      German: 'Silvester',
      Dutch: 'Silvester',
      Maltese: 'No-Name',
      Latin: 'Silvester',
      French: 'Sylvestre',
      Italian: 'Silvestro',
      Spanish: 'Silvestre',
      Catalan: 'Silvestre',
      Portuguese: 'Silvestre',
      Romanian: 'Silvester',
      
    }
  ],
  Simon: [
    {
      German: 'Simon',
      Dutch: 'Simon, Sijmen',
      Maltese: 'No-Name',
      Latin: 'Simonis',
      French: 'Simon',
      Italian: 'Simone',
      Spanish: 'Simón',
      Catalan: 'Simó',
      Portuguese: 'Simão',
      Romanian: 'Simion',
      
    }
  ],
  Solomon: [
    {
      German: 'Salomon',
      Dutch: 'Salomon',
      Maltese: 'No-Name',
      Latin: 'Salomo',
      French: 'Salomon',
      Italian: 'Salomone',
      Spanish: 'Salomón',
      Catalan: 'No-Name',
      Portuguese: 'Salomão',
      Romanian: 'Solomon',
      
    }
  ],
  Spyridon: [
    {
      German: 'No-Name',
      Dutch: 'Spiridon',
      Maltese: 'No-Name',
      Latin: 'Spyridon',
      French: 'Spiridione',
      Italian: 'Spiridione',
      Spanish: 'Espiridión',
      Catalan: 'No-Name',
      Portuguese: 'Espiridão',
      Romanian: 'Spiridon',
      
    }
  ],
  Stanley: [
    {
      German: 'Stanislaus',
      Dutch: 'Stanley, Stanislaus',
      Maltese: 'No-Name',
      Latin: 'Stanislaus',
      French: 'Stanislas',
      Italian: 'Stanislao',
      Spanish: 'Estanislao',
      Catalan: 'Estanislau',
      Portuguese: 'Estanislau',
      Romanian: 'No-Name',
      
    }
  ],
  'Steven, Stephen': [
    {
      German: 'Stephan, Stefan, Steffen',
      Dutch: 'Stefan, Steven',
      Maltese: 'No-Name',
      Latin: 'Stephanus',
      French: 'Étienne, Stéphane, Stève',
      Italian: 'Stefano',
      Spanish: 'Esteban',
      Catalan: 'Esteve',
      Portuguese: 'Estêvão',
      Romanian: 'Ştefan',
      
    }
  ],
  Taras: [
    {
      German: 'Tarus, Taurus',
      Dutch: 'No-Name',
      Maltese: 'Taras',
      Latin: 'No-Name',
      French: 'No-Name',
      Italian: 'No-Name',
      Spanish: 'No-Name',
      Catalan: 'No-Name',
      Portuguese: 'No-Name',
      Romanian: 'No-Name',
      
    }
  ],
  'Terence, Terrence': [
    {
      German: 'No-Name',
      Dutch: 'No-Name',
      Maltese: 'No-Name',
      Latin: 'Terentius',
      French: 'Térence',
      Italian: 'Terenzio',
      Spanish: 'Terencio',
      Catalan: 'Terenci',
      Portuguese: 'Terêncio',
      Romanian: 'No-Name',
      
    }
  ],
  'Theodore, Theo': [
    {
      German: 'Theodor, Theo',
      Dutch: 'Theodoor, Theo',
      Maltese: 'No-Name',
      Latin: 'Theodorus',
      French: 'Théodore, Théo',
      Italian: 'Teodoro, Teo',
      Spanish: 'Teodoro',
      Catalan: 'No-Name',
      Portuguese: 'Teodoro',
      Romanian: 'Tudor, Teodor',
      
    }
  ],
  'Thomas, Tom': [
    {
      German: 'Thomas, Thom, Tom',
      Dutch: 'Thomas',
      Maltese: 'No-Name',
      Latin: 'Thomas',
      French: 'Thomas',
      Italian: 'Tommaso',
      Spanish: 'Tomás',
      Catalan: 'No-Name',
      Portuguese: 'Tomás, Tomé',
      Romanian: 'Toma',
      
    }
  ],
  Tiberius: [
    {
      German: 'No-Name',
      Dutch: 'No-Name',
      Maltese: 'No-Name',
      Latin: 'Tiberius',
      French: 'Tibère',
      Italian: 'Tiberio',
      Spanish: 'Tiberio',
      Catalan: 'Tiberi',
      Portuguese: 'No-Name',
      Romanian: 'Tiberiu, Tibor',
      
    }
  ],
  'Timothy, Tim': [
    {
      German: 'Timotheus, Timo, Tim',
      Dutch: 'Timotheüs, Timo, Tim',
      Maltese: 'No-Name',
      Latin: 'Timotheus',
      French: 'Timothée',
      Italian: 'Timoteo',
      Spanish: 'Timoteo',
      Catalan: 'Timoteu',
      Portuguese: 'Timóteo',
      Romanian: 'Timoteu',
      
    }
  ],
  'Toby, Tobiah, Tobias': [
    {
      German: 'Tobias',
      Dutch: 'Tobias',
      Maltese: 'No-Name',
      Latin: 'Tobias',
      French: 'Tobie',
      Italian: 'Tobia',
      Spanish: 'Tobías',
      Catalan: 'No-Name',
      Portuguese: 'Tobias',
      Romanian: 'No-Name',
      
    }
  ],
  Urban: [
    {
      German: 'Urban',
      Dutch: 'Urbaan',
      Maltese: 'No-Name',
      Latin: 'Urbanus',
      French: 'Urbain',
      Italian: 'Urbano',
      Spanish: 'Urbano',
      Catalan: 'No-Name',
      Portuguese: 'Urbano',
      Romanian: 'Urban',
      
    }
  ],
  Ulric: [
    {
      German: 'Ulrich',
      Dutch: 'Ulrik',
      Maltese: 'No-Name',
      Latin: 'Ulricus',
      French: 'Ulric',
      Italian: 'Ulrico',
      Spanish: 'Ulrico',
      Catalan: 'No-Name',
      Portuguese: 'Ulrico',
      Romanian: 'Ulric',
      
    }
  ],
  Uriah: [
    {
      German: 'Urias',
      Dutch: 'Urias, Uria',
      Maltese: 'Urias',
      Latin: 'Uria',
      French: 'Urias',
      Italian: 'Uria',
      Spanish: 'No-Name',
      Catalan: 'No-Name',
      Portuguese: 'No-Name',
      Romanian: 'No-Name',
      
    }
  ],
  Uriel: [
    {
      German: 'Uriel',
      Dutch: 'Uriël',
      Maltese: 'No-Name',
      Latin: 'Ulriel',
      French: 'Uriël',
      Italian: 'Uriele',
      Spanish: 'Uriel',
      Catalan: 'Oriol',
      Portuguese: 'Uriel',
      Romanian: 'No-Name',
      
    }
  ],
  Ursus: [
    {
      German: 'Urs, Ursus',
      Dutch: 'Ursus',
      Maltese: 'No-Name',
      Latin: 'Ursus',
      French: 'Urs',
      Italian: 'Orsino, Orso',
      Spanish: 'Urso',
      Catalan: 'No-Name',
      Portuguese: 'Urso',
      Romanian: 'Urs',
      
    }
  ],
  Valentine: [
    {
      German: 'Valentin',
      Dutch: 'Valentijn',
      Maltese: 'No-Name',
      Latin: 'Valentinus',
      French: 'Valentin',
      Italian: 'Valentino',
      Spanish: 'Valentín',
      Catalan: 'Valentí',
      Portuguese: 'Valentim',
      Romanian: 'Valentin',
      
    }
  ],
  Valerian: [
    {
      German: 'Valerian',
      Dutch: 'Valeriaan',
      Maltese: 'No-Name',
      Latin: 'Valerianus',
      French: 'Valérien',
      Italian: 'Valeriano',
      Spanish: 'Valeriano',
      Catalan: 'No-Name',
      Portuguese: 'Valeriano',
      Romanian: 'No-Name',
      
    }
  ],
  Valerius: [
    {
      German: 'Valerius',
      Dutch: 'Valerius',
      Maltese: 'No-Name',
      Latin: 'Valerius',
      French: 'Valéry',
      Italian: 'Valerio',
      Spanish: 'Valerio, Valero',
      Catalan: 'Valeri',
      Portuguese: 'Valério',
      Romanian: 'Valeriu',
      
    }
  ],
  Victor: [
    {
      German: 'Viktor',
      Dutch: 'Victor, Viktor',
      Maltese: 'No-Name',
      Latin: 'Victor',
      French: 'Victor',
      Italian: 'Vittorio',
      Spanish: 'Víctor, Victorio',
      Catalan: 'Víctor',
      Portuguese: 'Victor, Vítor',
      Romanian: 'Victor',
      
    }
  ],
  Vincent: [
    {
      German: 'Vinzenz',
      Dutch: 'Vincent',
      Maltese: 'No-Name',
      Latin: 'Vincentius',
      French: 'Vincent',
      Italian: 'Vi(n)cenzo, Vincente',
      Spanish: 'Vicente',
      Catalan: 'Vicens',
      Portuguese: 'Vicente',
      Romanian: 'Vincențiu',
      
    }
  ],
  Vital: [
    {
      German: 'No-Name',
      Dutch: 'Vitalis',
      Maltese: 'No-Name',
      Latin: 'Vital',
      French: 'Vitalis',
      Italian: 'Vitale',
      Spanish: 'Vidal, Vital',
      Catalan: 'No-Name',
      Portuguese: 'Vital',
      Romanian: 'Vitalie',
      
    }
  ],
  Valdemar: [
    {
      German: 'Waldemar, Woldemar',
      Dutch: 'Waldemar',
      Maltese: 'Valdemar',
      Latin: 'No-Name',
      French: 'No-Name',
      Italian: 'Valdemaro',
      Spanish: 'Valdemaro',
      Catalan: 'No-Name',
      Portuguese: 'No-Name',
      Romanian: 'No-Name',
      
    }
  ],
  William: [
    {
      German: 'Wilhelm, Willy',
      Dutch: 'Wilhelm, Willem, Wim',
      Maltese: 'No-Name',
      Latin: 'Gulielmus, Guilhelmus, Gudlielmus',
      French: 'Guillaume',
      Italian: 'Guglielmo',
      Spanish: 'Guillermo',
      Catalan: 'Guillem',
      Portuguese: 'Guilherme',
      Romanian: 'No-Name',
      
    },
    {
      German: 'Wilhelm',
      Dutch: 'Willem',
      Maltese: 'No-Name',
      Latin: 'Gulielmus',
      French: 'Guillaume',
      Italian: 'Guglielmo',
      Spanish: 'Guillermo',
      Catalan: 'Guillem',
      Portuguese: 'Guilhermo',
      Romanian: 'Gulielm',
      
    }
  ],
  Vladimir: [
    {
      German: 'Wladimir',
      Dutch: 'Wladimir',
      Maltese: 'No-Name',
      Latin: 'Vladimirus',
      French: 'Wladimir',
      Italian: 'Vladimiro',
      Spanish: 'No-Name',
      Catalan: 'No-Name',
      Portuguese: 'Vladimiro',
      Romanian: 'Vladimir, Vlad',
      
    }
  ],
  Vladislav: [
    {
      German: 'Ladislaus, Wladislaus, Wladislaw, Vladislav, Vladislaus',
      Dutch: 'Ladislaus',
      Maltese: 'Ladislaus',
      Latin: 'Ladislas',
      French: 'Ladislao',
      Italian: 'Ladislao',
      Spanish: 'No-Name',
      Catalan: 'Ladislao',
      Portuguese: 'Ladislau',
      Romanian: 'No-Name',
      
    }
  ],
  Walter: [
    {
      German: 'Walther, Walter',
      Dutch: 'Walter, Wouter, Wolter',
      Maltese: 'No-Name',
      Latin: 'Gualterius',
      French: 'Gautier, Gauthier',
      Italian: 'Gualtiero, Walter',
      Spanish: 'Gualterio',
      Catalan: 'No-Name',
      Portuguese: 'Válter',
      Romanian: 'No-Name',
      
    }
  ],
  Wenceslas: [
    {
      German: 'Wenzel',
      Dutch: 'Wenceslaus',
      Maltese: 'No-Name',
      Latin: 'Wenceslaus',
      French: 'Wenceslas',
      Italian: 'Venceslao',
      Spanish: 'Wenceslao, Venceslao',
      Catalan: 'No-Name',
      Portuguese: 'Venceslau',
      Romanian: 'No-Name',
      
    }
  ],
  Wilfred: [
    {
      German: 'Wilfried',
      Dutch: 'Wilfred, Wilfried',
      Maltese: 'No-Name',
      Latin: 'Vilfridus',
      French: 'No-Name',
      Italian: 'Vilfrido',
      Spanish: 'Wilfredo',
      Catalan: 'Vilfredo',
      Portuguese: 'No-Name',
      Romanian: 'No-Name',
      
    }
  ],
  Xavier: [
    {
      German: 'Xaver',
      Dutch: 'Xavier',
      Maltese: 'No-Name',
      Latin: 'Xaverius',
      French: 'Xavier',
      Italian: 'Saverio',
      Spanish: 'Javier',
      Catalan: 'Xavier',
      Portuguese: 'Xavier',
      Romanian: 'No-Name',
      
    }
  ],
  Xenophon: [
    {
      German: 'No-Name',
      Dutch: 'Xenophon',
      Maltese: 'No-Name',
      Latin: 'Xenophon',
      French: 'Xénophon',
      Italian: 'Senofonte',
      Spanish: 'Jenofonte',
      Catalan: 'No-Name',
      Portuguese: 'Xenofonte',
      Romanian: 'No-Name',
      
    }
  ],
  'Zachary, Zack': [
    {
      German: 'Zacharias, Zach',
      Dutch: 'Zacharias, Zacharia',
      Maltese: 'No-Name',
      Latin: 'Zacharias',
      French: 'Zacharie',
      Italian: 'Zaccaria',
      Spanish: 'Zacarías',
      Catalan: 'No-Name',
      Portuguese: 'Zacarias',
      Romanian: 'Zaharia, Zaharie',
      
    }
  ]
}