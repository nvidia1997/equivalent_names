import { mappings } from "./mappings";
import { capitalize } from "./text-processor";

/**
* @returns {Array<String>} - list of names
*/
export const getNames = () => {
  return Object.keys(mappings);
}

/**
* @returns {Array<String>} - list of languages
*/
export const getLanguages = () => {
  const mappingKeys = getNames();
  const firstName = mappingKeys[0];
  const languages = mappings[firstName][0];

  return Object.keys(languages);
}

/**
 * @param {!String} name name in english
 * @param {!String} language language in english 
 */
export const getEquivalentName = (name, language) => {
  const key = capitalize(name);
  /**@type {Object} */
  const targetNameMap = mappings[key][0];
  const targetLanguage = capitalize(language);

  return targetNameMap[targetLanguage];
}
