import number_to_base from 'number-to-base';

/**
 * @param {!Array<Number>} numbers
 * @param {!Number} base -target base
 * @returns {Array<String>}
 */
export const convertNumbers = (numbers, base) => {
  return numbers.map(n => number_to_base(Number(n), base))
} 